% Script Math231_HW1 initializes a vector with zero coins of each 
% type (posVec), sets the amount of money we would like in change to 200
% cents, and sets up the vector with the coin denominations we need. We
% initialize a table to store and quickly look up coin combinations that 
% we have previously explored so avoid redundant searches. Finally the 
% count is initialized to zero and the makeChange function is called. The
% code takes about 95 seconds to produce an output.
posVec = [ 0 0 0 0 0 0 ];
max = 200;
curr = [100 50 25 10 5 1];
posHash = containers.Map();
count = 0;
tic
count = makeChange(curr, max, posHash, posVec, count)
toc