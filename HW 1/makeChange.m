function count = makeChange(curr, max, posHash, posVec, count)
% Funnction makeChange 
%    Exhaustively (and recursively) finds, counts, and outputs the 
%    number of ways to make change using the coin denominations provided
%    by the user.
%    
% Inputs: 
%       curr     A vector with the coin denominations that will
%                be used to create the max (total change)
%                Ex: curr = [100 50 25 10 5 1] for a 100 cent coin,
%                   a 50 cent coin, etc.
%
%       max      An integer value of the total amount of change
%                to be made using the coin denominations.
%                Ex: max = 200 for using change to make $2 or 200 cents
%
%       posHash  A 'table' of previously searched coin denominations.
%                (It allows for ignoring duplicates).
%
%       posVec   A vector containing the number of coins of each 
%                denomination.
%                Ex: posVec = [1 0 0 0 1 1], for having one 100 cent piece,
%                   one 5 cent piece, and one one cent piece. 
% Output:
%       count    The number of possible ways to make the desired amount
%                using the input coin denominations.
Identity = eye(length(curr));
% For loop branches an instance of posVec by separetly considering the
% addition of a coin for each coin denomination. Those branches are then
% explored and expanded through a recursive process until the posVec has
% desired quantity or exceeds the desired quantity.
for row = 1 : length(curr)
    % By adding a row of the identy matrix to the posVec (containing the
    % number of each coin) we add one coin of one denomination to posVec.
    tmpVec = Identity(row, :) + posVec;
    % The string version of the vector is used to search the table of
    % previously explored coin combinations.
    tmpVecStr = num2str(tmpVec);
    % If the coin combination has been seen before, then we do nothing.
    if isKey(posHash, tmpVecStr) 
    % If the tmpVec (a combination of coins) cointains less money than
    % we need in change, then we can keep adding coins by calling the
    % makeChange function. We also save the coin combinations to the 
    % table so that we don't search that combination again.
    elseif tmpVec * curr' < max
        posHash(tmpVecStr) = 1;
        count = makeChange(curr, max, posHash, tmpVec, count);
    % If the tmpVec (a combination of coins) cointains more money than
    % we need in change, then we save the posibility so that we do not
    % explore it again. 
    elseif tmpVec * curr' > max
        posHash(tmpVecStr) = 1;
    % Else is entered only if we have the exact amount of money that we
    % need using the given coin denominations. We increment the count
    % and store the coin combination.
    else
        count = count + 1;
        posHash(tmpVecStr) = 1;
    end
end